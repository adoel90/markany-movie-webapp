import { ReactNode } from 'react'


interface Props {
  children: ReactNode  
}

export default function AuthorLayout({ children }: Props) {  

  return (
    <>
      <div className="divide-y divide-gray-200 dark:divide-gray-700">        
       {children}
      </div>
    </>
  )
}
