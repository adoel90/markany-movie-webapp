'use client'
import { useRouter } from "next/navigation";
import { useDebounce } from "@/utils/hooks"

const Search= () => {

  const router = useRouter()

  const handleSearch = useDebounce((term) => {
    
    router.push(`/search-movie/${term}`)
  }, 1000);

  const handleChange = (event) => {
    const { value } = event.target;    
    
    if(value){
      handleSearch(value);
    }
  };


    return (

        <div className="flex flex-row gap-3 relative">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="h-5 md:h-6 w-5 md:w-6 text-gray-900 hover:text-red-500 dark:text-gray-100 right-[0.5rem] md:left-[0.5rem] top-[0.25rem] absolute"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
            />
          </svg>
          <input onChange={handleChange} type="search" className="hover:ring-offset-orange-red-500 h-7 md:h-8 rounded-lg w-44 md:w-72 max-w-md pl-2 md:pl-9 text-sm md:text-md border-gray-300 focus:border-red-500 focus:ring-red-500 focus:ring-[0.5px] text-red-500 " placeholder="Search Movies..." />
        </div>
      
    )
  }


export default Search