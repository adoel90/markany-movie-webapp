'use client';
import { useRef } from 'react';
import { useInView } from 'framer-motion';
import Link from './Link';
import MobileNav from './MobileNav';
import headerNavLinks from '@/data/headerNavLinks';


import Search from './Search';
import { fontMegrim } from '@/utils/font';
import { usePathname } from 'next/navigation'

const Header = () => {  

  const refYear = useRef(null);
  const isInViewYear = useInView(refYear, {
    once: true,
  });  
  
  const pathname = usePathname();  

  return (
    <header className="flex items-center justify-between py-10">
      <div className='flex items-center space-x-4 leading-5 sm:space-x-7 space-y-2'>
        <Link aria-label="Netflix" href="/">          
            <h5
              ref={refYear}
              className={`h-6 text-2xl text-red-500 font-semibold ${fontMegrim.className}`}
              style={{
                transform: isInViewYear ? 'none' : 'translateX(17px)',
                opacity: isInViewYear ? 1 : 0,
                transition: 'all 0.9s cubic-bezier(0.17, 0.55, 0.55, 1) 0.5s',
              }}
            >
              Markflix
            </h5>                                 
        </Link>      
                        
        {headerNavLinks
          .filter((link) => link.href !== '/')
          .map((link) => (
            <Link
              key={link.title}
              href={link.href}
              className={`hidden font-medium ${pathname == link.href ? "text-gray-900 underline" : "text-gray-500 "}  dark:text-gray-100 sm:block`}
            >
              {link.title}
            </Link>
          ))}
      </div>

      <div className="flex items-center space-x-4 leading-5 sm:space-x-6">
  
        <Search />        
        <MobileNav />        
      </div>
    </header>
  );
};

export default Header;
