'use client'
import { useState } from 'react'
import { useRouter } from 'next/navigation';
import { AnimatePresence, motion } from "framer-motion";
import YouTube, { YouTubeProps } from 'react-youtube';

const Modal = ({ data }) =>  {    

    const router = useRouter()
    const [modal, setModal ] = useState(true)

    const goBack = () => {
        
        setModal(false)
        router.back()
    }

    //
    const onPlayerReady: YouTubeProps['onReady'] = (event) => {        
        event.target.pauseVideo();
      }
    
 



    return (

      <AnimatePresence>
        {modal && (
          <div className="px-5 fixed h-full w-full flex items-center justify-center top-0 left-0 backdrop-blur">
            <motion.div
              className={`absolute z-10 p-5 ${data.success == false ? "bg-red-800" : "bg-black " } h-full md:h-3/4 w-full max-w-2xl rounded`}
              initial={{ y: 50, opacity: 0 }}
              animate={{
                y: 0,
                opacity: 1
              }}
              exit={{
                y: -50,
                opacity: 0
              }}
              transition={{ type: "spring", bounce: 0, duration: 0.4 }}
            >
              <button                
                onClick= {() => goBack()}
                className="absolute top-0 right-0 -mt-4 -mr-4 bg-white text-red-500 border border-red-500 h-8 w-8 block mb-2 rounded-full"
              >
                &times;
              </button>   

                  {
                    data.success == false ? 
                      <h3 className="mt-7 truncate text-xs md:text-2xl font-medium md:font-light text-black md:text-white text-center md:text-left ">
                          {data.status_message} -_-
                      </h3> : 
                      <section>
                          <div className="mt-3">
                           
                            <YouTube 
                                videoId={`${data?.videos?.results[data?.videos?.results.length - 1]?.key}`} 
                                opts={{
                                    
                                    height: '270',
                                    width: '100%',
                                    playerVars: {          
                                        autoplay: 0
                                    }
                                }} 
                                onReady={onPlayerReady} 
                            />
                        
                          </div>           

                          <h3 className="mt-7 truncate text-xs md:text-2xl font-medium md:font-light text-black md:text-white text-center md:text-left ">
                              {data.title}
                          </h3>
                          <p className="text-xs mt-3 text-white invisible md:visible">
                              {data.overview}
                          </p>
                      </section>
                  }
            
            </motion.div>
            <motion.div
              className="bg-transparent px-5 fixed h-full w-full flex items-center justify-center top-0 left-0"
              initial={{ opacity: 0 }}
              animate={{
                opacity: 1
              }}
              exit={{
                opacity: 0
              }}
              transition={{ type: "spring", bounce: 0, duration: 0.2 }}
              onClick= {() => goBack()}
            />
          </div>
        )}
      </AnimatePresence>
    );
  }
  
  export default Modal;