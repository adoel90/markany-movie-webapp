'use client'

import Link from '@/components/Link'

const Card = ({data}) => {
      
    return (

        <>
            <h3 className='text-md md:text-xl  font-medium mt-7 mb-2'>
                All Genre
            </h3>
            <article className="flex flex-row justify-between flex-wrap">
                {data.genres.map((item, index) => {

                    return (
                        <div key={index} className="grid grid-cols-12">
                            <Link  href={`/search-movie/${item.name}`} className="col-span-12" >   
                                <section className="hover:shadow-md p-1 m-1 bg-none md:bg-gray-50 border-gray-100 border-0 md:border-[1px]" >                      
                                    <h3 className="mt-2 text-xs md:text-lg font-medium md:font-light  truncate">                            
                                        {item.name}                             
                                    </h3>
                                </section>                                                                                                                                    
                            </Link>                                  
                        </div>

                        )
                    })}
            </article>                                 
        </>
    )

}

export default Card;