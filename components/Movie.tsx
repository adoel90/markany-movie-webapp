'use client'
import { Suspense } from 'react'
import { Splide, SplideSlide } from '@splidejs/react-splide'
import Link from '@/components/Link'

const LatestMovie = ({data, titleSection}) => {


    return (
            <section>
              <h3 className='text-md md:text-xl  font-medium mt-7 mb-2'>
                  {titleSection}
              </h3>

              <Splide 
                id="splide-latest"
                options={{                                                 
                  arrows: false,
                  type   : 'slide',
                  padding: '1rem',  
                  pagination:false,
                  perPage: 4,
                  height : '10rem',
                  rewind : true,       
                }}       
                style={{paddingLeft: "0px"}}              
                >
                  {data.results.filter(val => val.backdrop_path !== null).map((item, index) => {

                    return (
                      <SplideSlide  key={index} className="z-0">
                        <Link href={`/movie/${item.id}`}>                          
                            <figure className="p-1 m-1 bg-none md:bg-gray-50 border-gray-100 border-0 md:border-[1px] cursor-grabbing" >
                              <picture>
                                  <source  width="100%" height="200" media="(min-width: 1024px)" srcSet={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.backdrop_path}`} type="image/webpp" />
                                  <source  width="100%" height="150" media="(max-width: 640px)" srcSet={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.poster_path}`} type="image/png" />              
                                  <img className="object-contain rounded-sm" width="100%" height="200" loading="lazy" src={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.backdrop_path}`} alt={`${item.title}`} />
                              </picture>
                              <figcaption className="mt-2 text-xs md:text-md font-medium md:font-light  truncate">
                                <Suspense fallback={<i> &nbsp;Load...</i>}>
                                  {item.title} 
                                </Suspense>
                              </figcaption>
                            </figure>                                                                                                                                    
                        </Link>                      
                      </SplideSlide>

                    )
                  })}
              
              </Splide>
            </section>
    )
}

export default LatestMovie;