import Link from 'next/link'
import { slug } from 'github-slugger'
interface Props {
  text: string
}

const Tag = ({ text }: Props) => {
  return (
    <Link
      href={`/tags/${slug(text)}`}
      className="mr-3 text-sm font-medium uppercase text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
    >
      <span className="text-gray-900 dark:text-gray-100  first-letter:capitalize inline-block lowercase">When worked in</span> {text.split(' ').join('-')}
    </Link>
  )
}

export default Tag
