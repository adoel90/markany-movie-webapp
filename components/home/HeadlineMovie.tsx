'use client'

import { Splide, SplideSlide } from '@splidejs/react-splide'
import Link from '@/components/Link'

const HeadlineMovie = ({data}) => {
      
    return (
        <Splide 
            options={{                                                 
              arrows: false,
              type   : 'loop',
              padding: '5rem',                            
            }}            
        >
          {data.results.map((item, index) => {

            return (
              <SplideSlide  key={index} className="z-0 ">
                <Link href={`/movie/${item.id}`} >  
                  <article className="relative h-[270px] md:h-[357px] cursor-grabbing">
                    <section className="relative z-50 " >
                        <div className="grid grid-cols-12 place-content-end  h-auto md:h-[300px] ">
                          <div className="col-span-12 md:col-span-5">
                          </div>
                          <div className="col-span-12 md:col-span-7  ">
                            <div className="p-0 md:p-3 mr-0 md:mr-7 backdrop-blur-none md:backdrop-blur-sm rounded-sm">
                              <h3 className="truncate text-xs md:text-2xl font-medium md:font-light text-black md:text-white text-center md:text-left ">
                                {item.title}
                              </h3>
                              <p className="text-xs  text-gray-400 invisible md:visible">
                                {item.overview}
                              </p>
                            </div>
                          </div>
                        </div>
                    </section>
                    <div className="p-3 m-3 bg-none md:bg-gray-50 border-gray-100 border-0 md:border-[1px]  rounded-lg absolute top-0 left-0  ">
                      <picture>
                          <source  width="100%" height="424" media="(min-width: 1024px)" srcSet={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.backdrop_path}`} type="image/webpp" />
                          <source  width="100%" height="300" media="(max-width: 640px)" srcSet={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.poster_path}`} type="image/png" />              
                          <img className="object-contain rounded-sm cursor-grabbing" width="100%" height="424" loading="lazy" src={`${process.env.NEXT_PUBLIC_HOST_IMG_URL}${item.backdrop_path}`} alt={item.title} />
                      </picture>
                    </div>
                   
                  </article>
                                              
                  
                </Link>                      
              </SplideSlide>

            )
          })}
       
      </Splide>
    )

}

export default HeadlineMovie;