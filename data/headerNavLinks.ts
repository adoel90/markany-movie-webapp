const headerNavLinks = [
  { href: '/', title: 'Home' },
  { href: '/series-movies', title: 'Trending Tv Series' },
  { href: '/all-movies', title: 'Movies' },
  { href: '/all-genre', title: 'Genre' } 
]

export default headerNavLinks;



