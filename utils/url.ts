const getLastUrl = (pathUrl) => {
    
    const pathArray = pathUrl?.split('/');
    const lastPath = pathArray?.pop();    
    
    return lastPath;

}

const globalStore = () => {
    const memory = new Map();
  
    return {
      get pathname() {
        return memory.get("pathname");
      },
      set pathname(path: string) {
        memory.set("pathname", path);
      },
    };
  };

export const store = globalStore()


// Route handling

function removeTrailingSlash(path: string) {
    return path.endsWith("/") && path.length > 1 ? path.slice(0, -1) : path;
  }
  
  function getRouteAsPath(
    pathname: string,
    query: Record<string, string | number | string[] | number[]>,
    hash?: string | null | undefined
  ) {
    const remainingQuery = { ...query };
  
    // Replace slugs, and remove them from the `query`
    let asPath = pathname.replace(/\[{1,2}(.+?)]{1,2}/g, ($0, slug: string) => {
      if (slug.startsWith("...")) slug = slug.replace("...", "");
  
      const value = remainingQuery[slug]!;
      delete remainingQuery[slug];
      if (Array.isArray(value)) {
        return value.map((v) => encodeURIComponent(v)).join("/");
      }
      return value !== undefined ? encodeURIComponent(String(value)) : "";
    });
  
    // Remove any trailing slashes; this can occur if there is no match for a catch-all slug ([[...slug]])
    asPath = removeTrailingSlash(asPath);
  
    // Ensure query values are strings
    const record = Object.entries(remainingQuery).reduce<Record<string, string>>(
      (prev, [key, value]) => {
        prev[key] = [value].join("");
        return prev;
      },{}
    );
  
    // Append remaining query as a querystring, if needed:
    const qs = new URLSearchParams(record).toString();
  
    if (qs) asPath += `?${qs}`;
    if (hash) asPath += hash;
  
    return asPath;
  }
  
  const fromMetaUrlToPath = (
    metaUrl: string,
    query?: Record<string, string | number | string[] | number[]>
  ) => {
    const path = metaUrl.split("://")[1];
  
    return getRouteAsPath(
      removeTrailingSlash(
        path
          .replace(process.cwd(), "")
          .replace("/src/app", "")
          .replace("page.tsx", "")
      ),
      query ?? {}
    );
  };
  

export { getLastUrl, fromMetaUrlToPath }