const SPLIDE_CONFIG = {
    direction: 'ttb',
    height   : '100vh',    
    wheel: true,
    arrows: false    
  }
  

  export { SPLIDE_CONFIG }