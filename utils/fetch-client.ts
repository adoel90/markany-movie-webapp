// TODO: use this if need talk from client to server
import axios from 'axios'

export interface DefaultResData {
  status: boolean
  message: string
}

export interface DefaultTableReqData {
  lt?: number //limit
  of?: number //offset
  sb?: string //sort by
  ob?: 'asc' | 'desc' //order by
  search?: string
}

const baseURL = process.env.NEXT_PUBLIC_API_URL

const defaultOptions = {
  baseURL,
}

const client = axios.create(defaultOptions)

client.interceptors.response.use(
  (res) => {
    return res
  },
  (err) => {
    throw new Error(err.response.data.message)
  }
)

const clientPrivate = axios.create(defaultOptions)

clientPrivate.interceptors.request.use(async (req) => {
  // const session = await getSession()
  // if (session && req.headers) {
  //   req.headers.Authorization = `Bearer ${session.token}`
  // }
  return req
})

clientPrivate.interceptors.response.use(
  (res) => {
    return res
  },
  (err) => {
    if (err.response.status === 401) {
      // signOut()
    }
    throw new Error(err.response.data.message)
  }
)

// wordpress
const baseWPURL = process.env.NEXT_PUBLIC_WORDPRESS_API_URL

const clientWP = axios.create({
  baseURL: baseWPURL,
  method: 'post',
  headers: {
    'Content-Type': 'application/json',
  },
})

export { client, clientPrivate, clientWP }
