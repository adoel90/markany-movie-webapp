### Add or create .env file + "YOUR_MOVIE_DB_API_KEY" if you want starting this web app in your local computer
URL_API=https://api.themoviedb.org
API_KEY=YOUR_MOVIE_DB_API_KEY
NEXT_PUBLIC_HOST_IMG_URL=https://image.tmdb.org/t/p/original 


### TODO:
+ improve User-interaction
+ improve Metadata content
+ improve Security-config inside `next.config.js`
+ ,etc

