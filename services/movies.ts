
import { cache } from 'react'
import 'server-only'

const getGenre = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/genre/movie/list?api_key=${process.env.API_KEY}`);    
    return await res.json() ;
  
});


const getMoviesNowPlaying = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/movie/now_playing?api_key=${process.env.API_KEY}`);    
    return await res.json() ;
  
});

const getMovieLatest = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/trending/movie/day?api_key=${process.env.API_KEY}`);    
    return await res.json() ;
  
});

//id=28 is action genre
const getMovieActionGenre = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/discover/movie?api_key=${process.env.API_KEY}&with_genres=28`);    
    return await res.json() ;
  
});

const getMovieDetail = cache(async (id) => {

    const res = await fetch(`${process.env.URL_API}/3/movie/${id}?api_key=${process.env.API_KEY}&append_to_response=videos`);        
    return await res.json() ;
  
});

const getSearchMovie = cache(async (valueSearch) => {

    const res = await fetch(`${process.env.URL_API}/3/search/movie?query=${valueSearch}&api_key=${process.env.API_KEY}`);        
    return await res.json() ;
  
});

const getTrendingAllMovie = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/trending/all/day?api_key=${process.env.API_KEY}`);        
    return await res.json() ;
  
});

const getTrendingAllTvSeries = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/trending/tv/day?api_key=${process.env.API_KEY}`);        
    return await res.json() ;
  
});

const getRomanceMovies = cache(async () => {

    const res = await fetch(`${process.env.URL_API}/3/discover/movie?api_key=${process.env.API_KEY}&with_genres=10749`);         
    return await res.json() ;
  
});



export { getGenre, getMoviesNowPlaying, getMovieLatest, getMovieActionGenre, getMovieDetail, getSearchMovie, getTrendingAllMovie, getTrendingAllTvSeries, getRomanceMovies}