import { getMovieDetail } from '@/services/movies'
import Modal from '@/components/Modal'


export default async function Page ({params}){

    const {movie } = params;    

    const jsonData = await getMovieDetail(movie);
    const dataMovieDetail = await Promise.resolve(jsonData)
    
    return (
        
        <Modal data={dataMovieDetail} />
    )
}

