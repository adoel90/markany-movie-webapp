
import HeadlineMovie from '@/components/home/HeadlineMovie'
import { getTrendingAllMovie} from '@/services/movies'

export default async function Page() {
  
  const jsonData = await getTrendingAllMovie();
  const dataAllMovies = await Promise.resolve(jsonData)
  


  return (
    <>       
        <main className='flex h-screen flex-col justify-between'>                    

          <h3 className='text-md md:text-xl  font-medium mt-7 mb-2'>
                All Movies
          </h3>
          <section className=' mb-auto '>            
            <HeadlineMovie data={dataAllMovies ?? []} />
          </section>

                 

        </main>      
    </>
  )
}
