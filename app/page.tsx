
import HeadlineMovie from '@/components/home/HeadlineMovie'
import Movie from '@/components/Movie'
import { getMoviesNowPlaying, getMovieLatest, getMovieActionGenre, getRomanceMovies} from '@/services/movies'

export default async function Page() {
  
  const jsonData = await getMoviesNowPlaying();
  const dataMoviesNowPlaying = await Promise.resolve(jsonData)
  
  const jsonDataMovieLatest = await getMovieLatest()
  const dataMovieLatest = await Promise.resolve(jsonDataMovieLatest)

  const jsonDataMovieActionGenre = await getMovieActionGenre()
  const dataMovieActionGenre = await Promise.resolve(jsonDataMovieActionGenre)

  const jsonDataMovieRomanceGenre = await getRomanceMovies()
  const dataMovieRomanceGenre = await Promise.resolve(jsonDataMovieRomanceGenre)
    
  return (
    <>       
        <main className='flex h-screen flex-col justify-between'>                    
          <section className=' mb-auto '>            
            <HeadlineMovie data={dataMoviesNowPlaying ?? []} />
          </section>

          <article className="mb-auto">          
            <Movie
              titleSection="Action"
              data={dataMovieActionGenre ?? []}
             />       

            <Movie 
              titleSection="Latest"
              data={dataMovieLatest ?? []}
             />

            
          </article>          

        </main>      
    </>
  )
}
