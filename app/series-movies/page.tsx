
import HeadlineMovie from '@/components/home/HeadlineMovie'
import Movie from '@/components/Movie'
import { getTrendingAllTvSeries} from '@/services/movies'

export default async function Page() {
  
  const jsonData = await getTrendingAllTvSeries();
  const dataTrendingTvSeries = await Promise.resolve(jsonData)


  return (
    <>       
        <main className='flex h-screen flex-col justify-between'>                    
          <h3 className='text-md md:text-xl  font-medium mt-7 mb-2'>
                Series
          </h3>
          <section className=' mb-auto '>            
            <HeadlineMovie data={dataTrendingTvSeries ?? []} />
          </section>                

        </main>      
    </>
  )
}
