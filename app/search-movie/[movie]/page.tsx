import { getSearchMovie } from '@/services/movies'
import Movie from '@/components/Movie'

export default async function Page ({params}){

    const {movie } = params;    

    const jsonData = await getSearchMovie(movie);
    const dataResultSearchMovies = await Promise.resolve(jsonData)    

    return (
        
        <Movie titleSection="Result Search" data={dataResultSearchMovies ?? []} />
    )
}

