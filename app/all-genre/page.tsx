
import Card from '@/components/all-genre/Card'
import { getGenre } from '@/services/movies'

export default async function Page() {
  
  const jsonData = await getGenre();
  const dataAllGenre = await Promise.resolve(jsonData)

  console.log("dataAllGenre : ", dataAllGenre)

  return (
    <>       
        <main className='flex h-screen flex-col justify-between'>                              

          <article className="mb-auto">
             <Card data={dataAllGenre ?? []} />                       
          </article>          

        </main>      
    </>
  )
}
